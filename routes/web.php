<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['as' => '/', 'uses' => 'HomepageController@index']);
// Route::get('portfolio', ['as' => 'project', 'uses' => 'HomepageController@portfolio']);
Route::get('portfolio/{id}', ['as' => 'project/{id}', 'uses' => 'HomepageController@portfoliodetail']);

Route::get('login', ['as' => 'login', 'uses' => 'AuthController@index']);
Route::post('login', ['as' => 'login', 'uses' => 'AuthController@login']);
Route::get('logout', ['as' => 'logout', 'uses' => 'AuthController@logout']);

Route::middleware('loggedin:admin')->prefix('admin')->group(function () {
    Route::view('dashboard', 'admin.dashboard')->name('dashboard');
    Route::view('portfolio', 'admin.portfolio')->name('portfolio');
});
