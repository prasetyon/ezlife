<?php

namespace App\Http\Controllers;

use App\Models\Portfolio;
use App\Models\RefPortfolioType;
use Illuminate\Http\Request;

class HomepageController extends Controller
{
    //
    public function index()
    {
        $data['portfolio'] = Portfolio::orderBy('start', 'desc')->get();
        $data['type'] = RefPortfolioType::all();

        return view('welcome')
            ->with('data', $data)
            ->with('description', 'EZ-Life adalah pengembang aplikasi website, android, ios, maupun desktop yang mengutamakan kenyamanan client, ketepatan waktu, dan hasil yang maksimal dan optimal');
    }

    public function portfolio()
    {
        $data['portfolio'] = Portfolio::orderBy('start', 'desc')->get();
        $data['type'] = RefPortfolioType::all();

        return view('portfolio')
            ->with('data', $data)
            ->with('description', 'deskripsi');
    }

    public function portfoliodetail($id)
    {
        $data['portfolio'] = Portfolio::where('slug', $id)->first();
        $data['type'] = RefPortfolioType::all();

        return view('portfoliodetail')
            ->with('data', $data)
            ->with('description', $data['portfolio']->detail ?? "");
    }
}
