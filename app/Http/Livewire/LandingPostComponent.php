<?php

namespace App\Http\Livewire;

use App\Models\Portfolio;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class LandingPostComponent extends Component
{
    public $showAll = false;
    public $paginatedPerPages = 5;

    public function toggleShow()
    {
        $this->showAll = !$this->showAll;
    }

    public function render()
    {
        return view('livewire.landing-post-component', [
            'portfolio' => Portfolio::when(!$this->showAll, function ($query) {
                $query->limit(3);
            })->orderBy('start', 'desc')->get(),
        ]);
    }
}
