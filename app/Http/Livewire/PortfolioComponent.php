<?php

namespace App\Http\Livewire;

use App\Models\FilePortfolio;
use App\Models\Portfolio;
use App\Models\PortfolioType;
use App\Models\RefPortfolioType;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;

class PortfolioComponent extends Component
{
    // Load addon trait
    use WithPagination, WithFileUploads;

    // Bootsrap pagination
    protected $paginationTheme = 'bootstrap';

    // Public variable
    public $isOpen = 0;
    public $isOpenType = 0;
    public $paginatedPerPages = 10;
    public $input_id, $searchTerm, $input_detail, $input_name, $input_file,
        $input_start, $input_author, $input_client, $user, $input_url, $input_appstore,
        $input_playstore;
    public $photos = [];
    public $input_type;

    public function mount()
    {
        $this->user = Auth::id();
    }

    public function render()
    {
        $searchData = $this->searchTerm;
        return view('livewire.portfolio-component', [
            // Lists
            'listType' => RefPortfolioType::orderBy('name')->get(),
            'lists' => Portfolio::when($searchData, function ($searchQuery) use ($searchData) {
                $searchQuery->where([
                    ['detail', 'like', '%' . $searchData . '%']
                ])->orWhere([
                    ['name', 'like', '%' . $searchData . '%'],
                ])->orWhere([
                    ['author', 'like', '%' . $searchData . '%'],
                ])->orWhere([
                    ['client', 'like', '%' . $searchData . '%'],
                ]);
            })->orderBy('start', 'desc')->paginate($this->paginatedPerPages)
        ]);
    }

    // Reset input fields
    private function resetInputFields()
    {
        $this->reset([
            'input_id', 'input_detail', 'input_name', 'input_file',
            'input_client', 'input_author', 'input_start', 'input_type',
            'input_url', 'input_appstore', 'input_playstore', 'photos'
        ]);
    }

    public function closeModal()
    {
        $this->isOpen = false;
        $this->isOpenType = false;
    }

    public function create()
    {
        $this->isOpen = true;
        $this->isOpenType = false;
        $this->resetInputFields();
    }

    public function openType($id)
    {
        $this->input_id = $id;
        $this->isOpen = false;
        $this->isOpenType = true;
    }

    public function store()
    {
        $messages = [
            '*.required' => 'This column is required',
            '*.numeric' => 'This column is required to be filled in with number',
            '*.string' => 'This column is required to be filled in with letters',
        ];

        $this->validate([
            'input_name' => 'required|string',
            'input_detail' => 'required',
            'input_author' => 'required',
            'input_start' => 'required',
        ], $messages);

        // Insert or Update if Ok
        $data = Portfolio::updateOrCreate(['id' => $this->input_id], [
            'name' => $this->input_name,
            'detail' => $this->input_detail,
            'slug' => strtolower(preg_replace('/\s+/', '-', $this->input_name)),
            'author' => $this->input_author,
            'start' => $this->input_start,
            'client' => $this->input_client,
            'appstore' => $this->input_appstore,
            'playstore' => $this->input_playstore,
            'url' => $this->input_url,
            'created_by' => Auth::id(),
        ]);

        $id = (!$this->input_id) ? $data->id : $this->input_id;

        $i = 1;
        foreach ($this->photos as $photo) {
            $fileName = time() . ($i++) . '_' . strtolower(preg_replace('/\s+/', '_', $this->input_name));
            $photo->storeAs('portfolio', $fileName);

            FilePortfolio::create([
                'portfolio' => $id,
                'name' => $fileName,
                'created_by' => Auth::id(),
                'file' => env('APP_URL') . '/file/portfolio/' . $fileName,
            ]);
        }

        // Show an alert
        $this->alert('success', $this->input_id ? 'Data berhasil diperbarui' : 'Data berhasil disimpan');

        $this->closeModal();
    }

    public function storeType()
    {
        $messages = [
            '*.required' => 'This column is required',
            '*.numeric' => 'This column is required to be filled in with number',
            '*.string' => 'This column is required to be filled in with letters',
        ];

        $this->validate([
            'input_type' => 'required|string',
        ], $messages);

        // Insert or Update if Ok
        $data = PortfolioType::create([
            'portfolio' => $this->input_id,
            'type' => $this->input_type,
        ]);

        // Show an alert
        $this->alert('success', 'Data berhasil disimpan');

        $this->closeModal();
    }

    // Parse data to input form
    public function edit($id)
    {
        // Find data from the $id
        $data = Portfolio::findOrFail($id);

        // Parse data from the $data variable
        $this->input_id = $id;
        $this->input_name = $data->name;
        $this->input_detail = $data->detail;
        $this->input_author = $data->author;
        $this->input_client = $data->client;
        $this->input_start = $data->start;
        $this->input_appstore = $data->appstore;
        $this->input_playstore = $data->playstore;
        $this->input_url = $data->url;

        $this->isOpen = true;
        $this->isOpenType = false;
    }

    // Delete data
    public function deleteType($id)
    {
        $deltype = PortfolioType::where('id', $id)->delete();

        // Show an alert
        $this->alert('warning', 'Data berhasil dihapus');
    }

    // Delete data
    public function deleteFile($id)
    {
        $delfile = FilePortfolio::where('id', $id)->delete();

        // Show an alert
        $this->alert('warning', 'Data berhasil dihapus');
    }

    // Delete data
    public function delete($id)
    {
        // Find existing photo
        $del = Portfolio::where('id', $id)->firstOrFail();

        $deltype = PortfolioType::where('portfolio', $id)->delete();

        $delfile = FilePortfolio::select('file')->where('portfolio', $id)->get();
        foreach ($delfile as $sql) {
            unlink(storage_path('app/portfolio/' . substr(str_replace(env('APP_URL') . '/file/portfolio', "", $sql->file), 1)));
        }
        $delfile = FilePortfolio::where('portfolio', $id)->delete();
        $delete = Portfolio::where('id', $id)->delete();

        // Show an alert
        $this->alert('warning', 'Data berhasil dihapus');
    }
}
