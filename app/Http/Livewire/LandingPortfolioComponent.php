<?php

namespace App\Http\Livewire;

use App\Models\Portfolio;
use App\Models\RefPortfolioType;
use Livewire\Component;

class LandingPortfolioComponent extends Component
{
    public function render()
    {
        return view('livewire.landing-portfolio-component', [
            'type' => RefPortfolioType::orderBy('name')->get(),
            'portfolio' => Portfolio::orderBy('start', 'desc')->get()
        ]);
    }
}
