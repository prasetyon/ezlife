<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Portfolio extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function file()
    {
        return $this->hasMany(FilePortfolio::class, 'portfolio');
    }

    public function type()
    {
        return $this->hasMany(PortfolioType::class, 'portfolio');
    }

    public function creator()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function cover()
    {
        return $this->hasOne(FilePortfolio::class, 'portfolio')->first();
    }

    public function typeList()
    {
        $type = $this->type;
        $typeList = [];
        foreach ($type as $t) {
            $typeList[] = '"' . $t->parent->name . '"';
        }

        return implode(", ", $typeList);
    }
}
