<?php

namespace Database\Seeders;

use App\Models\RefPortfolioType;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        User::truncate();
        User::create([
            'name' => 'Prasetyo Nugrohadi',
            'username' => 'prasetyon',
            'role' => 'superuser',
            'password' => bcrypt('123'),
        ]);

        RefPortfolioType::truncate();
        RefPortfolioType::create([
            'name' => 'Website',
        ]);
        RefPortfolioType::create([
            'name' => 'Android',
        ]);
        RefPortfolioType::create([
            'name' => 'iOS',
        ]);
        RefPortfolioType::create([
            'name' => 'Logo',
        ]);
    }
}
