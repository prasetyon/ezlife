<!DOCTYPE html>
<html lang="en">
    @include("landing.layout.head")
    <body class="top" id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
        <!-- Preloader (Optional)-->
        <div id="preloader">
            <div id="status"></div>
        </div>
        @include("landing.layout.navbar")

        <!-- Portfolio Section -->
        @livewire('landing-portfolio-component')

        <!-- Services Section-->
        <section class="section-small text-center bg-gray" id="services">
            <div class="container">
                <div class="row">
                <h2>Our Services</h2>
                <div class="col-lg-3 col-sm-6 col-xs-6 wow fadeIn" data-wow-delay=".2s">
                    <h4><i class="ion-iphone icon-big"></i> Mobile Apps</h4>
                    {{-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla convallis pulvinar vestibulum. Donec eleifend, sem sed dictum</p> --}}
                </div>
                <div class="col-lg-3 col-sm-6 col-xs-6 wow fadeIn" data-wow-delay=".4s">
                    <h4><i class="ion-laptop icon-big"></i> Website</h4>
                    {{-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla convallis pulvinar vestibulum. Donec eleifend, sem sed dictum</p> --}}
                </div>
                <div class="col-lg-3 col-sm-6 col-xs-6 wow fadeIn" data-wow-delay=".6s">
                    <h4><i class="ion-ios-color-wand-outline icon-big"></i> DESIGN</h4>
                    {{-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla convallis pulvinar vestibulum. Donec eleifend, sem sed dictum</p> --}}
                </div>
                <div class="col-lg-3 col-sm-6 col-xs-6 wow fadeIn" data-wow-delay=".8s">
                    <h4><i class="ion-ios-clock-outline icon-big"></i> CONSULTING</h4>
                    {{-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla convallis pulvinar vestibulum. Donec eleifend, sem sed dictum</p> --}}
                </div>
                </div>
            </div>
        </section>

        <!-- Posts Section -->
        @livewire('landing-post-component')

        @include("landing.layout.footer")
        <a class="topbtn page-scroll" href="#page-top"></a>
        @include("landing.layout.js")
    </body>
</html>
