<!DOCTYPE html>
<html lang="en">
    @include("landing.layout.head")
    <body class="top" id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
        <!-- Preloader (Optional)-->
        <div id="preloader">
            <div id="status"></div>
        </div>
        @include("landing.layout.navbar")

        <!-- Portfolio-->
        <section id="portfolio">
            <div class="container text-center">
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1">
                        <h1 style="color: #4115F0; margin-bottom: 5px">EZ-Life Developer</h1>
                        <h4 style="margin-bottom: 5px">EZ Life, Better Future</h4>

                        <div class="list-inline" style="margin-top: 10px; margin-bottom:15px">
                            <a href="https://www.instagram.com/ezlifedev/"><i class="fab fa-instagram fa-fw fa-lg"></i></a>
                            <a href="https://play.google.com/store/apps/dev?id=4699635762255639002&hl=in&gl=US"><i class="fab fa-android la-fw fa-lg"></i></a>
                            <a href="https://apps.apple.com/id/developer/cv-grasia-prima-perfekta/id1551476836"><i class="fab fa-apple fa-fw fa-lg"></i></a>
                            {{--<a href="/"><i class="fab fa-youtube fa-fw fa-lg"></i></a>
                            <a href="/"><i class="fab fa-github-alt fa-fw fa-lg"></i></a> --}}
                            <a href="https://www.linkedin.com/company/ezlifedeveloper"><i class="fab fab fa-linkedin-in fa-fw fa-lg"></i></a>
                        </div>
                        @include("landing.layout.whatsapp")

                        <h5>
                            <ul class="portfolio-sorting list-inline text-center">
                                <li><a class="active" href="portfolio-single.html" data-group="all">All</a></li>
                                @foreach($data['type'] as $d)
                                <li><a href="portfolio-single.html" data-group="{{ $d->name }}">{{ $d->name }}</a></li>
                                @endforeach
                            </ul>
                        </h5>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row portfolio-items" id="grid">
                    @foreach ($data['portfolio'] as $d)
                    @if($loop->index>2) @break @endif
                    <div class="col-sm-6"><a href="news-single-sidebar.html"><img class="img-responsive center-block" src="{{ $d->cover()->file }}" alt="" style="width: 100%; height: 250px">
                        <h4>{{ $d->name }}</h4></a>
                        <p style="height: 100px">{{ Str::substr($d->detail, 0, 200).'...' }}</p><a class="btn btn-gray btn-xs" href="news-single.html">Read more</a>
                    </div>
                    @endforeach
                </div>
            </div>
        </section>
        <!-- Services Section-->
        <section class="section-small text-center bg-gray" id="services">
            <div class="container">
                <div class="row">
                <h2>Our Services</h2>
                <div class="col-lg-3 col-sm-6 wow fadeIn" data-wow-delay=".2s">
                    <h4><i class="ion-iphone icon-big"></i> Mobile Apps</h4>
                    {{-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla convallis pulvinar vestibulum. Donec eleifend, sem sed dictum</p> --}}
                </div>
                <div class="col-lg-3 col-sm-6 wow fadeIn" data-wow-delay=".4s">
                    <h4><i class="ion-laptop icon-big"></i> Website</h4>
                    {{-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla convallis pulvinar vestibulum. Donec eleifend, sem sed dictum</p> --}}
                </div>
                <div class="col-lg-3 col-sm-6 wow fadeIn" data-wow-delay=".6s">
                    <h4><i class="ion-ios-color-wand-outline icon-big"></i> DESIGN</h4>
                    {{-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla convallis pulvinar vestibulum. Donec eleifend, sem sed dictum</p> --}}
                </div>
                <div class="col-lg-3 col-sm-6 wow fadeIn" data-wow-delay=".8s">
                    <h4><i class="ion-ios-clock-outline icon-big"></i> CONSULTING</h4>
                    {{-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla convallis pulvinar vestibulum. Donec eleifend, sem sed dictum</p> --}}
                </div>
                </div>
            </div>
        </section>
         <!-- Posts Block-->
        <section class="section-small">
            <div class="container">
                <h2>Latest Project<a class="fa fa-plus-circle fa-fw gray" href="news3.html" title="See All"></a></h2>
                <div class="row grid-pad">
                    @foreach ($data['portfolio'] as $d)
                    @if($loop->index>2) @break @endif
                    <div class="col-sm-4"><a href="news-single-sidebar.html"><img class="img-responsive center-block" src="{{ $d->cover()->file }}" alt="" style="width: 100%; height: 250px">
                        <h4>{{ $d->name }}</h4></a>
                        <p style="height: 100px">{{ Str::substr($d->detail, 0, 200).'...' }}</p><a class="btn btn-gray btn-xs" href="news-single.html">Read more</a>
                    </div>
                    @endforeach
                    {{-- <div class="col-sm-4"><a href="news-single-sidebar.html"><img class="img-responsive center-block" src="img/main/23.jpg" alt="">
                        <h4>Consectetur elit</h4></a>
                        <p>Lorem ipsum dolor sit amet, consectetur elit. Nulla convallis pulvinar vestibulum. Donec eleifend, sem sed dictum mattis, turpis purus.</p><a class="btn btn-gray btn-xs" href="news-single.html">Read more</a>
                    </div>
                    <div class="col-sm-4"><a href="news-single-sidebar.html"><img class="img-responsive center-block" src="img/main/8.jpg" alt="">
                        <h4>Dolor sit amet</h4></a>
                        <p>Lorem ipsum dolor sit amet, consectetur elit. Nulla convallis pulvinar vestibulum. Donec eleifend, sem sed dictum mattis, turpis purus.</p><a class="btn btn-gray btn-xs" href="news-single.html">Read more</a>
                    </div> --}}
                </div>
            </div>
        </section>
        @include("landing.layout.footer")
        <a class="topbtn page-scroll" href="#page-top"></a>
        @include("landing.layout.js")
    </body>
</html>
