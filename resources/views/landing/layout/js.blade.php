<!-- jQuery-->
<script src="{{ asset('landing/js/jquery-3.3.1.min.js') }}"></script>
<!-- Bootstrap Core JavaScript-->
<script src="{{ asset('landing/js/bootstrap.min.js') }}"></script>
<!-- Plugin JavaScript-->
<script src="{{ asset('landing/js/jquery.easing.min.js') }}"></script>
<script src="{{ asset('landing/js/jquery.countdown.min.js') }}"></script>
<script src="{{ asset('landing/js/device.min.js') }}"></script>
<script src="{{ asset('landing/js/form.min.js') }}"></script>
<script src="{{ asset('landing/js/jquery.placeholder.min.js') }}"></script>
<script src="{{ asset('landing/js/jquery.shuffle.min.js') }}"></script>
<script src="{{ asset('landing/js/jquery.parallax.min.js') }}"></script>
<script src="{{ asset('landing/js/jquery.circle-progress.min.js') }}"></script>
<script src="{{ asset('landing/js/jquery.swipebox.min.js') }}"></script>
<script src="{{ asset('landing/js/wow.min.js') }}"></script>
<script src="{{ asset('landing/js/jquery.smartmenus.js') }}"></script>
<script src="{{ asset('landing/js/text-rotator.min.js') }}"></script>
<!-- Custom Theme JavaScript-->
<script src="{{ asset('landing/js/main.js') }}"></script>
@livewireScripts()
