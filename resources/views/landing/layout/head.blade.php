<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/png" href="{{ asset('favicon.ico') }}">
    <meta name="description" content="{{$description}}">
    <meta name="author" content="EZ-Life Developer">
    <meta name="facebook-domain-verification" content="rmbej651icxyo8pfepgnhn1dkzwm5z" />
    <meta property="og:description"
        content="EZ-Life adalah pengembang aplikasi website, android, ios, maupun desktop serta jasa translasi" />
    <meta property="og:url" content="https://ezlife.id" />
    <meta property="og:image" content="{{ asset('logo.jpg') }}" />
    <meta property="og:title" content="{{ env("APP_NAME") }}" />
    <meta property="og:type" content="portfolio" />
    <meta property="og:locale:alternate" content="in_ID" />

    <meta name="twitter:card" content="summary" />
    {{--
    <meta name="twitter:site" content="@ezlifedeveloper" />
    <meta name="twitter:creator" content="@ezlifedeveloper" /> --}}

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-2GP63H0BSB"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-2GP63H0BSB');
    </script>

    <title>{{ env("APP_NAME") }}</title>
    @include('landing/layout.css')
    @livewireStyles()
</head>
