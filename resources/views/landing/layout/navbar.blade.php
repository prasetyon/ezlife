<!-- Navigation-->
<nav class="navbar navbar-Concept navbar-center navbar-custom navbar-fixed-top nav-bright">
    <div class="container">
        <div class="row">
            <div class="navbar-header">
                {{-- <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".navbar-main-collapse"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button> --}}
                <a class="navbar-brand page-scroll" href="{{ route("/") }}">
                    <!-- Text or Image logo-->
                    <div class="logobig">
                        <img class="logo" src="{{ asset('logo.jpg') }}" alt="Logo">
                        <img class="logodark" src="{{ asset('logo.jpg') }}" alt="Logo">
                    </div>
                </a>
            </div>
        </div>
    </div>
</nav>
