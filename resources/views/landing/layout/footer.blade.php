<!-- footer-->
<section class="section-small bg-gray footer">
    <div class="container">
        <div class="row">
            <h5 class="no-pad">EZ-Life Developer &copy; 2017 - {{date('Y')}}</h5>
        </div>
    </div>
</section>
