@extends('admin', ['title' => 'Portfolio'])

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-9">
            @livewire('portfolio-component')
        </div>
        <div class="col-lg-3">
            @livewire('ref-portfolio-type-component')
        </div>
    </div>
</div>
@endsection
