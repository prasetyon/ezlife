@if($isOpen)
<div class="card">
    <div class="card-header">
        <button wire:click="closeModal()" class="btn btn-secondary"><i class="fas fa-angle-left pr-1"></i> Back</button>
    </div>
    <form wire:submit.prevent="store()">
        <div class="card-body">
            <!-- Select -->
            <div class="form-group col-12">
                <label for="input_name">Name</label>
                <input type="text" wire:model="input_name" id="input_name" class="form-control @error('input_name') is-invalid @enderror">
                @error('input_name') <div class="invalid-feedback">{{ $message }}</div> @enderror
            </div>

            <div class="form-group col-12">
                <label for="input_author">Author</label>
                <input type="text" wire:model="input_author" id="input_author" class="form-control @error('input_author') is-invalid @enderror">
                @error('input_author') <div class="invalid-feedback">{{ $message }}</div> @enderror
            </div>

            <div class="form-group col-12">
                <label for="input_client">Client</label>
                <input type="text" wire:model="input_client" id="input_client" class="form-control @error('input_client') is-invalid @enderror">
                @error('input_client') <div class="invalid-feedback">{{ $message }}</div> @enderror
            </div>

            <div class="form-group col-12">
                <label for="input_url">WEB URL</label>
                <input type="text" wire:model="input_url" id="input_url" class="form-control @error('input_url') is-invalid @enderror">
                @error('input_url') <div class="invalid-feedback">{{ $message }}</div> @enderror
            </div>

            <div class="form-group col-12">
                <label for="input_playstore">Play Store URL</label>
                <input type="text" wire:model="input_playstore" id="input_playstore" class="form-control @error('input_playstore') is-invalid @enderror">
                @error('input_playstore') <div class="invalid-feedback">{{ $message }}</div> @enderror
            </div>

            <div class="form-group col-12">
                <label for="input_appstore">App Store URL</label>
                <input type="text" wire:model="input_appstore" id="input_appstore" class="form-control @error('input_appstore') is-invalid @enderror">
                @error('input_appstore') <div class="invalid-feedback">{{ $message }}</div> @enderror
            </div>

            <div class="form-group col-12">
                <label for="input_detail">Detail</label>
                <textarea wire:model="input_detail" id="input_detail" class="form-control @error('input_detail') is-invalid @enderror" rows="10"></textarea>
                @error('input_detail') <div class="invalid-feedback">{{ $message }}</div> @enderror
            </div>

            <div class="form-group col-12">
                <label class="font-weight-bold">Project Started</label>
                <input type="date" wire:model="input_start" class="form-control @error('input_start') is-invalid @enderror">
                @error('input_start') <div class="invalid-feedback">{{ $message }}</div> @enderror
            </div>

            <div class="form-group col-12">
                <label for="input_file">File</label>
                <input type="file" wire:model="photos" class="form-control" multiple>
                @error('photos.*') <span class="error">{{ $message }}</span> @enderror
            </div>
        </div>
        <div class="card-footer text-right">
            <button type="reset" class="btn btn-danger">Reset</button>
            <button type="button" wire:click.prevent="store()" class="btn btn-success">Save</button>
        </div>
    </form>
</div>
@elseif($isOpenType)
<div class="card">
    <div class="card-header">
        <button wire:click="closeModal()" class="btn btn-secondary"><i class="fas fa-angle-left pr-1"></i> Back</button>
    </div>
    <form wire:submit.prevent="storeType()">
        <div class="card-body">
            <!-- Select -->
            <div class="form-group col-12">
                <label for="type">Portfolio Type</label>
                <select wire:model="input_type" class="form-control select2 @error('input_type') is-invalid @enderror" required="required">
                    <option value="" selected="selected">- Select -</option>
                    @foreach($listType as $t)
                        <option value="{{ $t->id }}">{{ $t->name }}</option>
                    @endforeach
                </select>
                @error('input_type') <div class="invalid-feedback">{{ $message }}</div> @enderror
            </div>
        </div>
        <div class="card-footer text-right">
            <button type="reset" class="btn btn-danger">Reset</button>
            <button type="button" wire:click.prevent="storeType()" class="btn btn-success">Save</button>
        </div>
    </form>
</div>
@else
<div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col-6">
                <button wire:click="create()" class="btn btn-dark"><i class="fas fa-plus pr-1"></i> Add New</button>
            </div>
            <div class="col-6">
                <input type="text" wire:model="searchTerm" placeholder="Search Something..." class="form-control">
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover">
                <h4>Portfolio Data</h4>
                <thead class="text-center">
                    <tr>
                        <th width="5%">No</th>
                        <th class="text-left">Name</th>
                        <th class="text-left">Type</th>
                        <th class="text-left">Detail</th>
                        <th class="text-left">File</th>
                        <th width="5%">Action</th>
                    </tr>
                </thead>
                <tbody class="text-center">
                    @forelse($lists as $list)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td class="text-left">
                            {{ $list->name }}<br>
                            by <b>{{ $list->author }}</b><br>
                            since <b>{{ $list->start }}</b><br>
                            @if($list->url)
                            <a href="{{$list->url}}"><img width="35px" height="35px" src="{{asset('weburl.png')}}"></a>
                            @endif
                            @if($list->playstore)
                            <a href="{{$list->playstore}}"><img width="35px" height="35px" src="{{asset('playstore.png')}}"></a>
                            @endif
                            @if($list->appstore)
                            <a href="{{$list->appstore}}"><img width="35px" height="35px" src="{{asset('appstore.png')}}"></a>
                            @endif
                        </td>
                        <td>
                            @foreach ($list->type as $t)
                                <p style="margin-bottom: 0">{{$t->parent->name}}
                                @if(in_array(Auth::user()->role, ['admin', 'superuser']))
                                <a wire:click="deleteType({{$t->id}})" style="color: red"
                                    wire:onclick="confirm('Are you sure to delete?') || event.stopImmediatePropagation()"> <i class="fas fa-trash"></i></a>
                                @endif
                                </p>
                            @endforeach
                            @if(in_array(Auth::user()->role, ['admin', 'superuser']))
                            <br>
                            <p wire:click="openType({{$list->id}})" style="color: blue;"> [assign new type]</p>
                            @endif
                        </td>
                        <td class="text-left" style="white-space:pre-wrap; word-wrap:break-word">{{ $list->detail }}</td>
                        <td>
                            @foreach ($list->file as $f)
                            <p>
                                <a href="{{$f->file}}" target="_blank">{{$f->name}}</a>
                                @if(in_array(Auth::user()->role, ['admin', 'superuser']))
                                <a wire:click="deleteFile({{$f->id}})" style="color: red"
                                    wire:onclick="confirm('Are you sure to delete?') || event.stopImmediatePropagation()"><i class="fas fa-trash"></i></a>
                                @endif
                            </p>
                            @endforeach
                        </td>
                        <td style="text-align: center;">
                            <button wire:click="edit({{ $list->id }})" class="btn btn-sm btn-info" style="width:100%; margin: 2px"><i class="fas fa-edit"></i></button>
                            <button wire:click="delete({{ $list->id }})" class="btn btn-sm btn-danger" style="width:100%; margin: 2px" onclick="confirm('Are you sure to delete?') || event.stopImmediatePropagation()"><i class="fas fa-trash"></i></button>
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="6">No Data Available</td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
        @if($lists->hasPages())
            {{ $lists->links() }}
        @endif
    </div>
</div>
@endif
