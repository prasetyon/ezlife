<div>
    <section class="section-small">
        <div class="container">
            <h2>Recent Post <a class="fa fa-{{ $showAll ? "minus" : "plus" }}-circle fa-fw gray" wire:click="toggleShow()" title="See All"></a></h2>
            <div class="row grid-pad">
                @foreach ($portfolio as $d)
                <div class="col-sm-4" style="margin-bottom: 50px">
                    <a href="{{ url('portfolio/'.$d->slug) }}">
                        <img class="img-responsive center-block" src="{{ $d->cover()->file }}" alt="" style="width: 100%; height: 250px; object-fit: contain;">
                        <h4 style="height: 45px">{{ $d->name }}</h4>
                    </a>
                    <p style="height: 80px">{{ Str::substr($d->detail, 0, 150).'...' }}</p>
                    <a class="btn btn-gray btn-xs" href="news-single.html">Read more</a>
                </div>
                @endforeach
            </div>
        </div>
    </section>
</div>
