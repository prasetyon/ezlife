<div>
    <section id="portfolio">
        <div class="container text-center">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1">
                    <h1 style="color: #4115F0; margin-bottom: 5px">EZ-Life Developer</h1>
                    <h4 style="margin-bottom: 5px">EZ Life, Better Future</h4>

                    <div class="list-inline" style="margin-top: 10px; margin-bottom:15px">
                        <a href="https://www.instagram.com/ezlifedev/"><i class="fab fa-instagram fa-fw fa-lg"></i></a>
                        <a href="https://play.google.com/store/apps/dev?id=4699635762255639002&hl=in&gl=US"><i class="fab fa-android la-fw fa-lg"></i></a>
                        <a href="https://apps.apple.com/id/developer/cv-grasia-prima-perfekta/id1551476836"><i class="fab fa-apple fa-fw fa-lg"></i></a>
                        {{-- <a href="/"><i class="fab fa-youtube fa-fw fa-lg"></i></a>
                        <a href="/"><i class="fab fa-github-alt fa-fw fa-lg"></i></a> --}}
                        <a href="https://www.linkedin.com/company/ezlifedeveloper"><i class="fab fab fa-linkedin-in fa-fw fa-lg"></i></a>
                    </div>
                    @include("landing.layout.whatsapp")

                    <h5>
                        <ul class="portfolio-sorting list-inline text-center">
                            <li style="width:auto"><a class="active" href="portfolio-single.html" data-group="all">All</a></li>
                            @foreach($type as $d)
                            <li style="width:auto"><a href="portfolio-single.html" data-group="{{ $d->name }}">{{ $d->name }}</a></li>
                            @endforeach
                        </ul>
                    </h5>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row portfolio-items shuffle" id="grid">
                @foreach($portfolio as $d)
                <div class="col-sm-3 no-pad" data-groups="[{{ $d->typeList() }}]">
                    <div class="portfolio-item">
                        <a href="{{ url('portfolio/'.$d->slug) }}"><img src="{{ $d->cover()->file }}" alt="{{ $d->name }}">
                            <div class="portfolio-overlay">
                                <div class="caption">
                                    <h5>{{ $d->name }}</h5><span>{{ str_replace('"', '', $d->typeList()) }}</span>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>
</div>
