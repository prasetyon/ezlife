<!DOCTYPE html>
<html lang="en">
    @include("landing.layout.head")
    <body class="top" id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
        <!-- Preloader (Optional)-->
        <div id="preloader">
            <div id="status"></div>
        </div>
        @include("landing.layout.navbar")

        <!-- ======= Portfolio Details Section ======= -->
        <section id="portfolio">
            <div class="container text-center">
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1">
                        <h1 style="color: #4115F0; margin-bottom: 5px">EZ-Life Developer</h1>
                        <h4 style="margin-bottom: 5px"><i>EZ Life, Better Future</i></h4>

                        <div class="list-inline" style="margin-top: 10px; margin-bottom:15px">
                            <a href="https://www.instagram.com/ezlifedev/"><i class="fab fa-instagram fa-fw fa-lg"></i></a>
                            <a href="https://play.google.com/store/apps/dev?id=4699635762255639002&hl=in&gl=US"><i class="fab fa-android la-fw fa-lg"></i></a>
                            <a href="https://apps.apple.com/id/developer/cv-grasia-prima-perfekta/id1551476836"><i class="fab fa-apple fa-fw fa-lg"></i></a>
                            {{--<a href="/"><i class="fab fa-youtube fa-fw fa-lg"></i></a>
                            <a href="/"><i class="fab fa-github-alt fa-fw fa-lg"></i></a> --}}
                            <a href="https://www.linkedin.com/company/ezlifedeveloper"><i class="fab fab fa-linkedin-in fa-fw fa-lg"></i></a>
                        </div>
                        @include("landing.layout.whatsapp")

                        <hr>
                        <h4 style="color: #4115F0">{{ $data['portfolio']->name }}</h4>
                        <hr>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row gy-4">
                    <div class="col-lg-8">
                        <div class="carousel slide carousel-fade" id="carousel-light10" data-ride="carousel">
                            <ol class="carousel-indicators">
                                @foreach ($data['portfolio']->file as $f)
                                <li @if($loop->index==0) class="active" @endif data-target="#carousel-light10" data-slide-to="{{ $loop->index }}"></li>
                                @endforeach
                            </ol>
                            <div class="carousel-inner" role="listbox">
                                @foreach ($data['portfolio']->file as $f)
                                <div class="item @if($loop->index==0) active @endif">
                                    <img src="{{$f->file}}" alt="{{$f->name}}" style="width: 100%; height: 600px; object-fit: contain;">
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="portfolio-info">
                            <h3>Project information</h3>
                            <ul>
                                <li><strong>Category</strong>: {{ str_replace('"', '', $data['portfolio']->typeList()) }}</li>
                                <li><strong>Client</strong>: {{ $data['portfolio']->client }}</li>
                                <li><strong>Project date</strong>: {{ $data['portfolio']->start }}</li>
                                <li><strong>Project URL</strong>: <a href="{{ $data['portfolio']->url }}">{{ $data['portfolio']->url }}</a></li>
                            </ul>
                        </div>
                        <div class="portfolio-description">
                            <h2>Project Detail</h2>
                            <p style="white-space:pre-wrap; word-wrap:break-word">{{ $data['portfolio']->detail }}</p>
                        </div>
                        <div class="portfolio-description">
                            <h2>Visit our project on</h2>

                            @if($data['portfolio']->url)
                            <a href="{{$data['portfolio']->url}}"><img width="100px" height="100px" style="margin:5px" src="{{asset('weburl.png')}}"></a>
                            @endif
                            @if($data['portfolio']->playstore)
                            <a href="{{$data['portfolio']->playstore}}"><img width="100px" height="100px" style="margin:5px" src="{{asset('playstore.png')}}"></a>
                            @endif
                            @if($data['portfolio']->appstore)
                            <a href="{{$data['portfolio']->appstore}}"><img width="100px" height="100px" style="margin:5px" src="{{asset('appstore.png')}}"></a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- End Portfolio Details Section -->
        <!-- Posts Block-->
        {{-- <section class="section-small">
            <div class="container">
                <h2>Latest Post<a class="fa fa-plus-circle fa-fw gray" href="news3.html" title="See All"></a></h2>
                <div class="row grid-pad">
                    @foreach ($data['portfolio'] as $d)
                    @if($loop->index>2) @break @endif
                    <div class="col-sm-4"><a href="news-single-sidebar.html"><img class="img-responsive center-block" src="{{ $d->cover()->file }}" alt="" style="width: 100%; height: 250px">
                        <h4>{{ $d->name }}</h4></a>
                        <p style="height: 100px">{{ Str::substr($d->detail, 0, 200).'...' }}</p><a class="btn btn-gray btn-xs" href="news-single.html">Read more</a>
                    </div>
                    @endforeach
                </div>
            </div>
        </section> --}}
        @include("landing.layout.footer")
        <a class="topbtn page-scroll" href="#page-top"></a>
        @include("landing.layout.js")
    </body>
</html>
